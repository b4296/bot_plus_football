package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"github.com/jasonlvhit/gocron"
)

// id XXX (1-4 цифр) + 2000000000 чат (указан тестовый)
const Peer_id = "2000000146"
const text = "%2B" // "+"

func main() {

	fmt.Println(os.Args[1])

	gocron.Every(1).Tuesday().At("9:00:00").Do(apiReq)
	// gocron.Every(1).Minute().Do(apiReq)
	<-gocron.Start()

}

func apiReq() {

	time_now := time.Now()
	// mess := text + time_now.Format(time.RFC3339Nano)

	request := "https://api.vk.com/method/messages.send?access_token=" + os.Args[1] + "&peer_id=" + Peer_id + "&message=" + text + "&random_id=0&v=5.131"

	client := &http.Client{}
	req, _ := http.NewRequest("GET", request, nil)
	resp, err := client.Do(req)

	if err != nil {
		fmt.Println(err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Printf("answer:%v   time= %v", string(body), time_now.Format(time.RFC3339Nano))
}
