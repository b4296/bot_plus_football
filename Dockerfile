FROM golang:alpine3.15

ENV TZ=Europe/Moscow


ENV VK_TOKENN="empty"

COPY . /go/bot_vk

WORKDIR /go/bot_vk


RUN go mod tidy && \
    go mod vendor && \
    env CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -mod=vendor -o ./bot_vk ./*go

ENTRYPOINT ./bot_vk ${VK_TOKENN}